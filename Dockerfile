FROM node:lts-alpine3.13
WORKDIR /home/node/app
COPY . ./

USER node
EXPOSE 3000
ENTRYPOINT [ "npm", "run", "start" ]